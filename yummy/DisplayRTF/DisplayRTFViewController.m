//
//  DisplayRTFViewController.m
//  yummy
//
//  Created by Dalia on 22/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import "DisplayRTFViewController.h"
#import "AppDelegate.h"

@interface DisplayRTFViewController ()

@end

@implementation DisplayRTFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.webView loadData:self.data MIMEType:@"text/rtf" textEncodingName:@"UTF-8" baseURL:nil];
    self.webView.scalesPageToFit = YES;
    self.webView.delegate = self;
    
    [self.activityIndicator setHidesWhenStopped:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.activityIndicator startAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.activityIndicator stopAnimating];
    if(error)
        [AppDelegate displayUserErrorMessage:error];
}

-(void)webViewDidFinishLoad:(UIWebView *)webVieww
{
    
    if(!webVieww.loading)
    {
        [self.activityIndicator stopAnimating];
    }
}

@end
