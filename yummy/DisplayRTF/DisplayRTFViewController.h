//
//  DisplayRTFViewController.h
//  yummy
//
//  Created by Dalia on 22/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayRTFViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSData *data;
@end
