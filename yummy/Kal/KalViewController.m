/* 
 * Copyright (c) 2009 Keith Lazuka
 * License: http://www.opensource.org/licenses/mit-license.html
 */
#import "KalViewController.h"
#import "KalLogic.h"
#import "KalDataSource.h"
#import "KalDate.h"
#import "KalPrivate.h"
#import "KalGridView.h"
#import "AppDelegate.h"

#define PROFILER 0
#if PROFILER
#include <mach/mach_time.h>
#include <time.h>
#include <math.h>
void mach_absolute_difference(uint64_t end, uint64_t start, struct timespec *tp)
{
    uint64_t difference = end - start;
    static mach_timebase_info_data_t info = {0,0};

    if (info.denom == 0)
        mach_timebase_info(&info);
    
    uint64_t elapsednano = difference * (info.numer / info.denom);
    tp->tv_sec = elapsednano * 1e-9;
    tp->tv_nsec = elapsednano - (tp->tv_sec * 1e9);
}
#endif

NSString *const KalDataSourceChangedNotification = @"KalDataSourceChangedNotification";

@interface KalViewController ()
@property (nonatomic, retain, readwrite) NSDate *initialDate;
@property (nonatomic, retain, readwrite) NSDate *selectedDate;
@property (nonatomic) UIView *backgroundDatePicker;
@property (nonatomic) UIDatePicker *datePicker;
@property (nonatomic) UIToolbar* keyboardDoneButtonView;
- (KalView*)calendarView;
@end

@implementation KalViewController

@synthesize dataSource, delegate, initialDate, selectedDate,backgroundDatePicker,datePicker,keyboardDoneButtonView;

- (id)initWithSelectedDate:(NSDate *)date
{
    if ((self = [super init])) {
        logic = [[KalLogic alloc] initForDate:date];
        self.initialDate = date;
        self.selectedDate = date;
        self.dataSource = [MealCalendarDataSource new];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(significantTimeChangeOccurred) name:UIApplicationSignificantTimeChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:KalDataSourceChangedNotification object:nil];
  }
  return self;
}

- (id)init
{
    return [self initWithSelectedDate:[NSDate date]];
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    return [self initWithSelectedDate:[NSDate date]];
}

- (KalView*)calendarView { return (KalView*)self.view; }

- (void)setDataSource:(id<KalDataSource>)aDataSource
{
  if (dataSource != aDataSource) {
    dataSource = aDataSource;
    tableView.dataSource = dataSource;
  }
}

- (void)setDelegate:(id<UITableViewDelegate>)aDelegate
{
  if (delegate != aDelegate) {
    delegate = aDelegate;
    tableView.delegate = delegate;
  }
}

- (void)clearTable
{
  [dataSource removeAllItems];
  [tableView reloadData];
}

- (void)reloadData
{
    NSLog(@"logicFrom %@, logicTo %@", logic.fromDate, logic.toDate);
    [dataSource presentingDatesFrom:logic.fromDate to:logic.toDate delegate:self];
    [self clearTable];
    [self didSelectDate:[KalDate dateFromNSDate:self.selectedDate]];
}

- (void)significantTimeChangeOccurred
{
  [[self calendarView] jumpToSelectedMonth];
  [self reloadData];
}

// -----------------------------------------
#pragma mark KalViewDelegate protocol

- (void)didSelectDate:(KalDate *)date
{
  self.selectedDate = [date NSDate];
    
    if([date NSDate])
    {
        NSComparisonResult result = [self compareDate:[date NSDate] andDate:[NSDate date]];
        if (result == NSOrderedDescending || result == NSOrderedSame)
            [self.navigationItem.rightBarButtonItem setEnabled:YES];
        else
            [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }
    
//    NSDate *from = [[date NSDate] cc_dateByMovingToBeginningOfDay];
//    NSDate *to = [[date NSDate] cc_dateByMovingToEndOfDay];
    
  [self clearTable];
  [dataSource loadItemsFromDate:[date NSDate] toDate:[date NSDate]];
  [tableView reloadData];
  [tableView flashScrollIndicators];
}

- (void)saveRecipeWithDate:(KalDate *)date
{
    NSComparisonResult result = NSOrderedAscending;
    if([date NSDate])
        result = [self compareDate:[date NSDate] andDate:[NSDate date]];
    
    NSDate *d = [self.plannedRecipe valueForKey:@"when"];
    
    if([self isPlannedRecipe] && (result == NSOrderedSame || result == NSOrderedDescending))
    {
        NSManagedObjectContext *context = [self managedObjectContext];
        if(!d)
        {
            [self.plannedRecipe setValue:selectedDate forKey:@"when"];
            [self displayAlertForPlanningRecipe];
        }
        else
        {
            NSLog(@"%@ %@", self.selectedDate, [self.plannedRecipe valueForKey:@"when"]);
            NSComparisonResult result = [self compareDate:self.selectedDate andDate:[self.plannedRecipe valueForKey:@"when"]];
            if(result == NSOrderedSame)
            {
                [self.plannedRecipe setValue:nil forKey:@"when"];
                [self deleteLocalNotificationWithID:self.plannedRecipe.notifID];
                [self displayAlertForPlanningRecipe];
            }
        }
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error])
        {
            NSLog(@"Can't plan a time for this recipe! %@ %@", error, [error localizedDescription]);
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Couldn't plan time for %@. Close the app and retry.", [self.plannedRecipe valueForKey:@"title"]] message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self reloadData];
    }
}

- (BOOL)isPlannedRecipe
{
    if(self.plannedRecipe)
        return YES;
    
    return NO;
}

- (void)showPreviousMonth
{
  [self clearTable];
  [logic retreatToPreviousMonth];
  [[self calendarView] slideDown];
  [self reloadData];
}

- (void)showFollowingMonth
{
  [self clearTable];
  [logic advanceToFollowingMonth];
  [[self calendarView] slideUp];
  [self reloadData];
}

// -----------------------------------------
#pragma mark KalDataSourceCallbacks protocol

- (void)loadedDataSource:(id<KalDataSource>)theDataSource;
{
  NSArray *markedDates = [theDataSource markedDatesFrom:logic.fromDate to:logic.toDate];
  NSMutableArray *dates = [[markedDates mutableCopy] autorelease];
  for (int i=0; i<[dates count]; i++)
    [dates replaceObjectAtIndex:i withObject:[KalDate dateFromNSDate:[dates objectAtIndex:i]]];
  
  [[self calendarView] markTilesForDates:dates];
  [self didSelectDate:self.calendarView.selectedDate];
}

// ---------------------------------------
#pragma mark -

- (void)showAndSelectDate:(NSDate *)date
{
  if ([[self calendarView] isSliding])
    return;
  
  [logic moveToMonthForDate:date];
  
#if PROFILER
  uint64_t start, end;
  struct timespec tp;
  start = mach_absolute_time();
#endif
  
  [[self calendarView] jumpToSelectedMonth];
  
#if PROFILER
  end = mach_absolute_time();
  mach_absolute_difference(end, start, &tp);
  printf("[[self calendarView] jumpToSelectedMonth]: %.1f ms\n", tp.tv_nsec / 1e6);
#endif
  
  [[self calendarView] selectDate:[KalDate dateFromNSDate:date]];
  [self reloadData];
}

- (NSDate *)selectedDate
{
  return [self.calendarView.selectedDate NSDate];
}


// -----------------------------------------------------------------------------------
#pragma mark UIViewController

- (void)didReceiveMemoryWarning
{
  self.initialDate = self.selectedDate; // must be done before calling super
  [super didReceiveMemoryWarning];
}

- (void)loadView
{
  if (self.plannedRecipe)
  {
      [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                           forBarMetrics:UIBarMetricsDefault];
      CGFloat screenWidth = [AppDelegate getScreenWidth];
      UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth-73, 44)];
      UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(-65, 0, screenWidth-73, 44)];
      label.backgroundColor = [UIColor clearColor];
      label.numberOfLines = 2;
      label.font = [UIFont systemFontOfSize:16.0f];
      label.textAlignment = NSTextAlignmentCenter;
      label.textColor = [UIColor blackColor];
      label.text = [self.plannedRecipe valueForKey:@"title"];
      [headerView addSubview:label];
      self.navigationItem.titleView = headerView;

  }
    
    CGRect fr = [[UIScreen mainScreen] applicationFrame];
    
    if (self.plannedRecipe)
        fr.origin.y += 45;
    
  KalView *kalView = [[[KalView alloc] initWithFrame:fr delegate:self logic:logic] autorelease];
  self.view = kalView;
  tableView = kalView.tableView;
  tableView.dataSource = dataSource;
  tableView.delegate = delegate;
  [tableView retain];
  [kalView selectDate:[KalDate dateFromNSDate:self.initialDate]];
  [self reloadData];
}

- (void)viewDidUnload
{
  [super viewDidUnload];
  [tableView release];
  tableView = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  [tableView flashScrollIndicators];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    if(self.plannedRecipe)
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"alarmIcon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(setAlarm:)]];
}

#pragma mark -

- (void)dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationSignificantTimeChangeNotification object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self name:KalDataSourceChangedNotification object:nil];
  [initialDate release];
  [selectedDate release];
  [logic release];
  [tableView release];
  [super dealloc];
}

# pragma mark - Core Data

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id del = [[UIApplication sharedApplication] delegate];
    if ([del performSelector:@selector(managedObjectContext)]) {
        context = [del managedObjectContext];
    }
    return context;
}

#pragma mark - Set alarm for current recipe
- (BOOL)setAlarm:(id)sender
{
    if ([self.plannedRecipe valueForKey:@"when"])
    {
        [self createPickerAndOther];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info" message:@"Please first select the date by holding press a day." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    return TRUE;
}

- (void)pickerChanged:(id)sender
{
    NSLog(@"value: %@",[sender date]);
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self dismissPickerViews];
}

- (void)doneCalClicked:(id)sender
{
    // save date and time for recipe
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    
    // get components from picker date
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitMinute | NSCalendarUnitHour fromDate:self.datePicker.date];
    NSInteger minute = [components minute];
    NSInteger hour = [components hour];
    
    // get components from recipe  date
    NSDateComponents *com = [[NSCalendar currentCalendar] components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[self.plannedRecipe valueForKey:@"when"]];
    NSInteger day = [com day];
    NSInteger month = [com month];
    NSInteger year = [com year];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:day];
    [comps setMonth:month];
    [comps setYear:year];
    [comps setMinute:minute];
    [comps setHour:hour];
    
    NSDate *dateToSet = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    [self.plannedRecipe setValue:dateToSet forKey:@"when"];
    
    [self setupLocalNotifWithDate:self.datePicker.date andText:[NSString stringWithFormat:@"Your recipe %@ is ready!", [self.plannedRecipe valueForKey:@"title"]]];
    if (![context save:&error])
    {
        NSLog(@"Can't plan a time for %@ recipe! %@ %@", [self.plannedRecipe valueForKey:@"title"], error, [error localizedDescription]);
    }
    else
    {
        [self displayAlertForPlanningRecipe];
    }
    
    [self dismissPickerViews];
    [tableView reloadData];
}

#pragma mark -

- (void)dismissPickerViews
{
    [self.backgroundDatePicker removeFromSuperview];
    [self.datePicker removeFromSuperview];
    [self.keyboardDoneButtonView removeFromSuperview];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

- (void)createPickerAndOther
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    CGFloat screenWidth = screenRect.size.width;
    
    if (self.datePicker == nil)
        self.datePicker = [UIDatePicker new];
    
    // calculate position of date picker
    CGFloat y = screenHeight - self.datePicker.frame.size.height;
    [self.datePicker setFrame:CGRectMake(0, y, self.datePicker.frame.size.width, self.datePicker.frame.size.height)];
    
    [self.datePicker setBackgroundColor:[UIColor whiteColor]];
    [self.datePicker addTarget:self action:@selector(pickerChanged:)               forControlEvents:UIControlEventValueChanged];
    self.datePicker.datePickerMode = UIDatePickerModeTime;
    [self.datePicker setDate:[NSDate date]];
    
    // Added DONE button to date picker toolbar
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissPickerViews)];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    if(self.keyboardDoneButtonView == nil)
        self.keyboardDoneButtonView = [UIToolbar new];
    [self.keyboardDoneButtonView setFrame:CGRectMake(0, y-25, self.keyboardDoneButtonView.frame.size.width, 25)];
    [self.keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                      style:UIBarButtonItemStyleBordered target:self
                                                     action:@selector(doneCalClicked:)];
    
    [self.keyboardDoneButtonView setItems:[NSArray arrayWithObjects:cancelButton,flexibleSpace, doneButton, nil]];
    
    
    if(self.backgroundDatePicker == nil)
        self.backgroundDatePicker = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, y)];
    [self.backgroundDatePicker setBackgroundColor:[UIColor lightGrayColor]];
    [self.backgroundDatePicker setAlpha:0.7];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.backgroundDatePicker addGestureRecognizer:singleFingerTap];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    [self.view addSubview:self.backgroundDatePicker];
    [self.view addSubview:self.datePicker];
    [self.view addSubview:self.keyboardDoneButtonView];
}

#pragma mark - local notif

- (void)setupLocalNotifWithDate:(NSDate*)date andText:(NSString*)text
{
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.fireDate = date;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    // Notification details
    localNotif.alertBody = text;
    // Set the action button
    localNotif.alertAction = @"View";
    localNotif.soundName = @"alarmSound.wav";
    NSString* storedUUIDString = [self createUUID];
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:storedUUIDString forKey:@"UUID"];
    localNotif.userInfo = infoDict;
    self.plannedRecipe.notifID = storedUUIDString;

    // Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

// Delete the specific local notification
- (void) deleteLocalNotificationWithID:(NSString*)storedUUIDString {
    // Fast enumerate to pick out the local notification with the correct UUID
    for (UILocalNotification *localNotification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if ([[localNotification.userInfo valueForKey:@"UUID"] isEqualToString: storedUUIDString]) {
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ; // delete the notification from the system
        }
    }
}

// Create a unique identifier to allow the local notification to be identified
- (NSString *)createUUID {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

- (NSComparisonResult)compareDate:(NSDate*)d1 andDate:(NSDate*)d2
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
    
    NSDateComponents *date1Components = [calendar components:comps
                                                    fromDate: d1];
    NSDateComponents *date2Components = [calendar components:comps
                                                    fromDate: d2];
    
    NSDate *d_1 = [calendar dateFromComponents:date1Components];
    NSDate *d_2 = [calendar dateFromComponents:date2Components];
    
    return [d_1 compare:d_2];
}

- (void)displayAlertForPlanningRecipe
{
    UIAlertView *alert;
    if([self.plannedRecipe valueForKey:@"when"])
    {
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[self.plannedRecipe valueForKey:@"when"]];
        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
         NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        if (hour && minute)
            [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
        else
            [formatter setDateFormat:@"dd-MM-yyyy"];
        
        NSString *dateOfRecipe =[formatter stringFromDate:[self.plannedRecipe valueForKey:@"when"]];
        
        alert = [[UIAlertView alloc]initWithTitle:@"Success" message:[NSString stringWithFormat:@"%@ is planned for %@.",[self.plannedRecipe valueForKey:@"title"], dateOfRecipe] delegate:nil cancelButtonTitle:@"Great" otherButtonTitles:nil, nil];
    }
    else
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Success" message:[NSString stringWithFormat:@"%@ was removed from meal calendar.",[self.plannedRecipe valueForKey:@"title"]] delegate:nil cancelButtonTitle:@"Great" otherButtonTitles:nil, nil];
    }
    [alert show];
}

@end
