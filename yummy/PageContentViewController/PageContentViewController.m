//
//  PageContentViewController.m
//  yummy
//
//  Created by Dalia on 05/12/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import "PageContentViewController.h"
#import "MainViewController.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.backgroundImageView.image = [UIImage imageNamed:self.imageFile];
    self.titleLabel.text = self.titleText;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
//- (IBAction)swipe:(id)sender
//{
//    NSLog(@"%d...",self.pageIndex);
//    if(self.pageIndex == 3)
//    {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//        MainViewController *mainVC = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
//        mainVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//
//        [self presentViewController:mainVC animated:YES completion:nil];
//    }
//}
//
//- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
//{
//    return YES;
//}
//
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
//{
//    return YES;
//}
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
//{
//    return YES;
//}
- (IBAction)skipToApp:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MainViewController *mainVC = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    mainVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:mainVC animated:YES completion:nil];
}
@end