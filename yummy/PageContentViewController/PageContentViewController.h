//
//  PageContentViewController.h
//  yummy
//
//  Created by Dalia on 05/12/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController<UIGestureRecognizerDelegate>
- (IBAction)skipToApp:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;
@end