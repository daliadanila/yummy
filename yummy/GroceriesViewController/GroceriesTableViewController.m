//
//  GroceriesTableViewController.m
//  yummy
//
//  Created by Dalia on 06/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "GroceriesTableViewController.h"
#import "MainViewController.h"

NSString *const kYourCustomMailType = @"YourCustomMailType";

@interface GroceriesTableViewController ()
{
    NSManagedObjectContext *context;
    NSMutableArray *ingredientsArray;
    UITextField *nIngredient;
    UIButton *addButton;
    UITableViewHeaderFooterView *header;
    CGFloat keyboardHeight;
}
@end

@implementation GroceriesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    header = [self.tableView headerViewForSection:1];
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Fetch the recipes from persistent data store
    context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Grocery"];
    ingredientsArray = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - keyboard methods

- (void)keyboardDidShow: (NSNotification *) notif
{
    CGSize keyboardSize = [[[notif userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    
    [self.tableView setFrame:CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height - keyboardHeight + self.tabBarController.tabBar.frame.size.height)];
}

- (void)keyboardWillHide: (NSNotification *)notif
{
    [self.tableView setFrame:CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height + keyboardHeight - self.tabBarController.tabBar.frame.size.height)];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ingredientsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if([self.editButton.title isEqualToString:@"Edit"])
        return 0;
    return 40.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    /* Create custom view to display section header... */
    nIngredient = [[UITextField alloc] initWithFrame:CGRectMake(15, 5, tableView.frame.size.width - 55, 40)];
    [nIngredient setFont:[UIFont systemFontOfSize:15]];
    nIngredient.placeholder = @"Type ingredient";
    nIngredient.delegate= self;
    [view addSubview:nIngredient];
    
    // Adding button "Add"
    addButton = [[UIButton alloc]initWithFrame:CGRectMake(tableView.frame.size.width - 50, 5, 50, 40)];
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(addedIngredient:) forControlEvents:UIControlEventAllTouchEvents];
    [view addSubview:addButton];
    
    [view setBackgroundColor:[UIColor whiteColor]];
    [view setAlpha:1];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"groceryCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    NSManagedObject *ingredient = ingredientsArray[indexPath.row];
    cell.textLabel.text = [ingredient valueForKey:@"ingredient"];
    cell.textLabel.numberOfLines = 0;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle) editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // Delete object from database
        [context deleteObject:[ingredientsArray objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Can't delete! %@ %@", error, [error localizedDescription]);
        }
        else
        {
            // Delete the row from the data source
            
            [ingredientsArray removeObjectAtIndex:indexPath.row];
            
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSString *stringToMove = ingredientsArray[sourceIndexPath.row];
    [ingredientsArray removeObjectAtIndex:sourceIndexPath.row];
    [ingredientsArray insertObject:stringToMove atIndex:destinationIndexPath.row];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.editing == YES)
        return YES;
    return NO;
}

-(NSIndexPath*)indexPathForNextRow
{
    return [NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:self.tableView.numberOfSections - 1] inSection:self.tableView.numberOfSections - 1];
}

- (IBAction)showMore:(id)sender
{
    // Must delete all instances
    if([self.showMoreButton.title isEqualToString:@"Delete All"])
    {
        NSFetchRequest * allIngredients = [[NSFetchRequest alloc] init];
        [allIngredients setEntity:[NSEntityDescription entityForName:@"Grocery" inManagedObjectContext:context]];
        [allIngredients setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error = nil;
        NSArray * ingredients = [context executeFetchRequest:allIngredients error:&error];
        for (NSManagedObject * ingredient in ingredients)
        {
            [context deleteObject:ingredient];
        }
        
        if (![context save:&error])
        {
            NSLog(@"Can't delete! %@ %@", error, [error localizedDescription]);
        }
        else
        {
            [ingredientsArray removeAllObjects];
            [self.tableView reloadData];

            //[self editIngredient:self.editButtonItem];
        }
    }
    
    // Share ingredients
    else
    {
        NSLog(@"%@", [ingredientsArray valueForKey:@"ingredient"] );
        // Open share sheet
        NSString *activityItems = [[[ingredientsArray valueForKey:@"ingredient"] valueForKey:@"description"] componentsJoinedByString:@"\n"];
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObject:activityItems] applicationActivities:nil];
        [self presentViewController:activityController animated:YES completion:nil];
    }
}

- (IBAction)editIngredient:(id)sender
{
    if([self.editButton.title isEqualToString:@"Edit"])
    {
        [self setEditing:YES];
        [nIngredient becomeFirstResponder];
        [self.editButton setTitle:@"Done"];
        [self.showMoreButton setTitle:@"Delete All"];
    }
    else
    {
        [self setEditing:NO];
        
        [nIngredient resignFirstResponder];
        [self.editButton setTitle:@"Edit"];
        [self.showMoreButton setTitle:@"Share"];
    }
        [self.tableView reloadData];
}

- (void)addedIngredient:(id)sender
{
    NSString *value = [nIngredient.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(value.length != 0)
    {
        // Add a new ingredient
        
        NSManagedObject *newIngredient = [NSEntityDescription insertNewObjectForEntityForName:@"Grocery" inManagedObjectContext:context];
        [newIngredient setValue:nIngredient.text forKey:@"ingredient"];
        
        // Save the object to persistent store
        
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        else
        {
            [ingredientsArray addObject:newIngredient];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[self indexPathForNextRow]] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    nIngredient.text = @"";

}

# pragma mark - Core Data

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *managedObjectContext = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        managedObjectContext = [delegate managedObjectContext];
    }
    return managedObjectContext;
}

# pragma mark - Custom Activity Controller

-(id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    NSDictionary *exportDetailsDict = [NSDictionary dictionary];
    if ([activityType isEqualToString:kYourCustomMailType]) {
        //-- create the attachment data
        NSData *attachementData = [NSData data];
        //-- message for the email
        NSString *theMessage = [NSString stringWithFormat:@"Some message:\n%@",ingredientsArray];
        //-- subject for the email
        NSString *theSubject = @"Email Subject";
        //-- create the dictionary
        exportDetailsDict = [NSDictionary dictionaryWithObjectsAndKeys:attachementData,@"ATTACHMENT",
                             theMessage,@"MESSAGE",
                             theSubject,@"SUBJECT", nil];
    }
    return exportDetailsDict;
}

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return [NSDictionary dictionary];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

@end
