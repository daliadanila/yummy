//
//  GroceriesTableViewController.h
//  yummy
//
//  Created by Dalia on 06/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface GroceriesTableViewController : UITableViewController<UITextFieldDelegate>

// UI
- (IBAction)showMore:(id)sender;
- (IBAction)editIngredient:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *showMoreButton;

@end
