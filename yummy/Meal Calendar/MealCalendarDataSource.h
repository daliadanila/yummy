//
//  MealCalendarDataSource.h
//  yummy
//
//  Created by Dalia on 10/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KalDataSource.h"
#import "../CoreData/Recipe.h"

@interface MealCalendarDataSource : NSObject <KalDataSource,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *items;
    NSMutableArray *recipes;
}

+ (MealCalendarDataSource *)dataSource;
- (Recipe *)recipeAtIndexPath:(NSIndexPath *)indexPath;
@end
