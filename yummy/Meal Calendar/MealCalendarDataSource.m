//
//  MealCalendarDataSource.m
//  yummy
//
//  Created by Dalia on 10/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import "MealCalendarDataSource.h"

static BOOL IsDateBetweenInclusive(NSDate *date, NSDate *begin, NSDate *end)
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
    
    NSDateComponents *date1Components = [calendar components:comps
                                                    fromDate: date];
    NSDateComponents *date2Components = [calendar components:comps
                                                    fromDate: begin];
    NSDateComponents *date3Components = [calendar components:comps
                                                    fromDate: end];
    
    date = [calendar dateFromComponents:date1Components];
    begin = [calendar dateFromComponents:date2Components];
    end = [calendar dateFromComponents:date3Components];
    
    if(([date compare:begin] != NSOrderedAscending && [date compare:end] != NSOrderedDescending) || ([date compare:begin] == NSOrderedSame && [date compare:end] == NSOrderedSame))
        return YES;
    return NO;
}

@interface MealCalendarDataSource()
- (NSArray *)recipesFrom:(NSDate *)fromDate to:(NSDate *)toDate;
@end

@implementation MealCalendarDataSource
+ (MealCalendarDataSource *)dataSource
{
    return [[[self class] alloc] init];
}

- (id)init
{
    if ((self = [super init])) {
        items = [[NSMutableArray alloc] init];
        recipes = [[NSMutableArray alloc] init];
    }
    return self;
}

- (Recipe *)recipeAtIndexPath:(NSIndexPath *)indexPath
{
    return [items objectAtIndex:indexPath.row];
}

#pragma mark UITableViewDataSource protocol conformance

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    Recipe *recipe = [self recipeAtIndexPath:indexPath];
    cell.imageView.image = [UIImage imageWithData:recipe.picture];
    cell.textLabel.text = recipe.title;
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel setFont:[UIFont systemFontOfSize:15]];
    
    
    if([recipe valueForKey:@"when"])
    {
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"HH:mm"];
        NSString *dateString = [dateFormatter stringFromDate:recipe.when];
        
        cell.detailTextLabel.text = @"";
        if(![dateString isEqualToString:@"00:00"])
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Alarm setted for %@", dateString];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [items count];
}

#pragma mark Core Data access

- (void)loadRecipesFrom:(NSDate *)fromDate to:(NSDate *)toDate delegate:(id<KalDataSourceCallbacks>)delegate
{
    NSLog(@"Fetching recipes from the database between %@ and %@...", fromDate, toDate);
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Recipe"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(when >= %@) AND (when <= %@)", fromDate, toDate];
    
    [fetchRequest setPredicate:predicate];
    recipes = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    [delegate loadedDataSource:self];
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *managedObjectContext = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        managedObjectContext = [delegate managedObjectContext];
    }
    return managedObjectContext;
}

#pragma mark KalDataSource protocol conformance

- (void)presentingDatesFrom:(NSDate *)fromDate to:(NSDate *)toDate delegate:(id<KalDataSourceCallbacks>)delegate
{
    [recipes removeAllObjects];
    [self loadRecipesFrom:fromDate to:toDate delegate:delegate];
}

- (NSArray *)markedDatesFrom:(NSDate *)fromDate to:(NSDate *)toDate
{
    return [[self recipesFrom:fromDate to:toDate] valueForKeyPath:@"when"];
}

- (void)loadItemsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate
{
    [items addObjectsFromArray:[self recipesFrom:fromDate to:toDate]];
}

- (void)removeAllItems
{
    [items removeAllObjects];
}

#pragma mark -

- (NSArray *)recipesFrom:(NSDate *)fromDate to:(NSDate *)toDate
{
    NSMutableArray *matches = [NSMutableArray array];
    for (Recipe *recipe in recipes)
        if (IsDateBetweenInclusive(recipe.when, fromDate, toDate))
            [matches addObject:recipe];
    
    return matches;
}


@end
