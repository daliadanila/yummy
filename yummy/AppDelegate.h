//
//  AppDelegate.h
//  yummy
//
//  Created by Dalia on 10/08/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)instantiateMainViewController;

+ (void)colorButtonsFromActionSheet:(UIActionSheet*)actionSheet;
+ (void)displayUserErrorMessage:(NSError*)error;
+ (CGFloat)getScreenHeight;
+ (CGFloat)getScreenWidth;

@end

