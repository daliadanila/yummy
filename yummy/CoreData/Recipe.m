//
//  Recipe.m
//  yummy
//
//  Created by Dalia on 14/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import "Recipe.h"
#import "Grocery.h"


@implementation Recipe

@dynamic addedToGroceryList;
@dynamic favourite;
@dynamic howTo;
@dynamic ingredients;
@dynamic picture;
@dynamic portions;
@dynamic time;
@dynamic title;
@dynamic when;
@dynamic groceryRelation;
@dynamic notifID;

@end
