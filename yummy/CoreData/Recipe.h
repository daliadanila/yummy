//
//  Recipe.h
//  yummy
//
//  Created by Dalia on 14/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Grocery;

@interface Recipe : NSManagedObject

@property (nonatomic, retain) NSNumber * addedToGroceryList;
@property (nonatomic, retain) NSNumber * favourite;
@property (nonatomic, retain) NSString * howTo;
@property (nonatomic, retain) NSString * ingredients;
@property (nonatomic, retain) NSData * picture;
@property (nonatomic, retain) NSString * portions;
@property (nonatomic, retain) NSString * time;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * notifID;
@property (nonatomic, retain) NSDate * when;
@property (nonatomic, retain) Grocery *groceryRelation;

@end
