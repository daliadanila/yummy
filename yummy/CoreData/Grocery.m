//
//  Grocery.m
//  yummy
//
//  Created by Dalia on 14/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import "Grocery.h"
#import "Recipe.h"


@implementation Grocery

@dynamic ingredient;
@dynamic recipeRelation;

@end
