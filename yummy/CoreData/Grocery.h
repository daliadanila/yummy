//
//  Grocery.h
//  yummy
//
//  Created by Dalia on 14/10/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Recipe;

@interface Grocery : NSManagedObject

@property (nonatomic, retain) NSString * ingredient;
@property (nonatomic, retain) NSSet *recipeRelation;
@end

@interface Grocery (CoreDataGeneratedAccessors)

- (void)addRecipeRelationObject:(Recipe *)value;
- (void)removeRecipeRelationObject:(Recipe *)value;
- (void)addRecipeRelation:(NSSet *)values;
- (void)removeRecipeRelation:(NSSet *)values;

@end
