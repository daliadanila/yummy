//
//  MainViewController.h
//  yummy
//
//  Created by Dalia on 11/08/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "SWTableViewCell.h"

@interface MainViewController : UITableViewController< SWTableViewCellDelegate, UISearchBarDelegate, UIActionSheetDelegate,MFMailComposeViewControllerDelegate, UIGestureRecognizerDelegate, UIWebViewDelegate>

+(BOOL)insertRecipesInContext:(NSManagedObjectContext*)context;

- (IBAction)openActionSheet:(id)sender;
- (IBAction)displayFavourite:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *favouriteButton;

@end
