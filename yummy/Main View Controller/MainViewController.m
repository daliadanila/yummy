//
//  MainViewController.m
//  yummy
//
//  Created by Dalia on 11/08/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#define SYSTEM_VERSION_LESS_THAN(v)   ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#import <CoreData/CoreData.h>
#import "KalViewController.h"
#import "MainViewController.h"
#import "WriteRecipeController.h"
#import "AppDelegate.h"
#import "DisplayRTFViewController.h"

@interface MainViewController ()
{
    NSMutableArray *recipesArray;
    NSMutableArray *searchResults;
    NSManagedObjectContext *context;
    int favs;
}

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    context = [self managedObjectContext];
    
    [self fetchAllRecipes];
    self.tableView.rowHeight = 90;
    favs = 0;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(favs == 0)
        [self.tableView reloadData];
    
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.searchDisplayController isActive])
    {
        return [searchResults count];
    }
    else
    {
        return [recipesArray count];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cell selected at index path %ld:%ld", (long)indexPath.section, (long)indexPath.row);
    NSLog(@"selected cell index path is %@", [self.tableView indexPathForSelectedRow]);
    
    [self performSegueWithIdentifier:@"showDetails" sender:nil];
    
    if (!tableView.isEditing) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    SWTableViewCell *cell = (SWTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell.layer setOpaque:YES];
    NSArray *objectsArray;
    
    if (cell == nil) {
        
        cell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
        cell.leftUtilityButtons = [self leftButtons];
        cell.rightUtilityButtons = [self rightButtons];
        cell.delegate = self;
    }
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        objectsArray = searchResults;
    }
    else
    {
        objectsArray = recipesArray;
    }
    
    Recipe *dateObject = objectsArray[indexPath.row];
    cell.textLabel.text = [dateObject valueForKey:@"title"];
    [cell.textLabel setFont:[UIFont systemFontOfSize:16]];
    cell.textLabel.backgroundColor = [UIColor whiteColor];
    cell.textLabel.numberOfLines = 3;
    
    cell.detailTextLabel.text = @"";
    cell.detailTextLabel.backgroundColor = [UIColor whiteColor];
    if (![[dateObject valueForKey:@"time"] isEqualToString:@"unknown"] && ![[dateObject valueForKey:@"time"] isEqualToString:@"Add time"] && [dateObject valueForKey:@"time"])
        cell.detailTextLabel.text = [dateObject valueForKey:@"time"];
    
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.imageView.image = [UIImage imageNamed:@"empty_picture.png"];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *img = [UIImage imageWithData:[dateObject valueForKey:@"picture"]];
        dispatch_sync(dispatch_get_main_queue(), ^{
            cell.imageView.image = img;
        });
    });
    
//    NSData *imageData = [[NSData alloc] initWithData:[dateObject valueForKey:@"picture"]];
//    
//    int imageSize = imageData.length;
//    NSLog(@"SIZE OF IMAGE: %i for %@", imageSize, [dateObject valueForKey:@"title"]);
    
    if([[objectsArray[indexPath.row] valueForKey:@"favourite"] isEqual:[NSNumber numberWithBool:YES]])
    {
        [cell.leftUtilityButtons[2] setImage:[UIImage imageNamed:@"red_heart.png"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.leftUtilityButtons[2] setImage:[UIImage imageNamed:@"empty_heart.png"] forState:UIControlStateNormal];
    }
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

#pragma mark - Search Display Controller Delegate Methods

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"title contains[c] %@", searchText];
    
    searchResults = [NSMutableArray arrayWithArray:[recipesArray filteredArrayUsingPredicate:resultPredicate]];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchDisplayController.active = NO;
    [self.tableView reloadData];
}

#pragma mark - SWCell Delegate Methods

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:@"Share"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Delete"];
    
    return rightUtilityButtons;
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor whiteColor] icon:[UIImage imageNamed:@"calAndTime.png"]];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:0.5] icon:[UIImage imageNamed:@"addToTrolley.png"]];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"empty_heart.png"]];
    return leftUtilityButtons;
}


#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    Recipe *selectedRecipe;
    NSInteger cellIndex;
    if (self.searchDisplayController.active)
    {
        cellIndex = [self.searchDisplayController.searchResultsTableView indexPathForCell:cell].row;
        selectedRecipe = searchResults[cellIndex];
    }
    else
    {
        cellIndex = [self.tableView indexPathForCell:cell].row;
        selectedRecipe = recipesArray[cellIndex];
    }
    
    switch (index) {
        case 0:
        {
            NSLog(@"left button 0 was pressed");
            NSDate *plannedRecipeDate = [selectedRecipe valueForKey:@"when"];
            KalViewController *calendar;
            if(plannedRecipeDate)
                calendar = [[KalViewController alloc] initWithSelectedDate:plannedRecipeDate];
            else
                calendar = [[KalViewController alloc] init];
            
            calendar.plannedRecipe = selectedRecipe;

            [self.navigationController pushViewController:calendar animated:YES];
            [self.tabBarController.tabBar setHidden:YES];
        }
            break;
        case 1:
        {
            NSLog(@"left button 1 was pressed");
            [self addIngredintsFromRecipe:selectedRecipe];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success!" message:@"Ingredients for this recipes were added to your grocery list!" delegate:nil cancelButtonTitle:@"Great" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        case 2:
        {
            NSLog(@"left button 2 was pressed");
            [self addFavouriteRecipe:selectedRecipe fromCell:cell];
            
            if(favs == 1)
                [self.tableView reloadData];
            if([[selectedRecipe valueForKey:@"favourite"] isEqual:[NSNumber numberWithBool:YES]])
                NSLog(@"DA----------------");
        }
            break;
        default:
            break;
    }
}

- (void)addIngredintsFromRecipe:(Recipe*)selectedRecipe
{
    // Add to ingredients list
    NSArray* ingredients = [[selectedRecipe valueForKey:@"ingredients"] componentsSeparatedByString: @"\n"];
    for (NSString *ingredient in ingredients)
    {
        NSString *value = [ingredient stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if(value.length != 0)
        {
            // Add a new ingredient
            
            NSManagedObject *newIngredient = [NSEntityDescription insertNewObjectForEntityForName:@"Grocery" inManagedObjectContext:context];
            [newIngredient setValue:ingredient forKey:@"ingredient"];
            
            // Save the object to persistent store
            
            NSError *error = nil;
            if (![context save:&error]) {
                NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
                [AppDelegate displayUserErrorMessage:error];
            }
        }
    }
}

- (void)addFavouriteRecipe:(Recipe*)selectedRecipe fromCell:(SWTableViewCell*)cell
{
    NSString *msgg;
    
    if([[selectedRecipe valueForKey:@"favourite"] isEqual:[NSNumber numberWithBool:YES]])
    {
        [selectedRecipe setValue:[NSNumber numberWithBool:NO] forKey:@"favourite"];
        [cell.leftUtilityButtons[2] setImage:[UIImage imageNamed:@"empty_heart.png"] forState:UIControlStateNormal];
        
        msgg = @"Removed recipe from favourites";
        
    }
    else
    {
        [selectedRecipe setValue:[NSNumber numberWithBool:YES] forKey:@"favourite"];
        [cell.leftUtilityButtons[2] setImage:[UIImage imageNamed:@"red_heart.png"] forState:UIControlStateNormal];
        
        msgg = @"Moved recipe to favourites";
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
        [AppDelegate displayUserErrorMessage:error];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success!" message:msgg delegate:nil cancelButtonTitle:@"Great" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    Recipe *selectedRecipe;
    NSInteger cellIndex;
    if (self.searchDisplayController.active)
    {
        cellIndex = [self.searchDisplayController.searchResultsTableView indexPathForCell:cell].row;
        selectedRecipe = searchResults[cellIndex];
    }
    else
    {
        cellIndex = [self.tableView indexPathForCell:cell].row;
        selectedRecipe = recipesArray[cellIndex];
    }
    
    switch (index) {
        case 0:
        {
            [self shareRecipe:selectedRecipe];
            
            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        case 1:
        {
            [self removeRecipe:selectedRecipe inCell:cell];
            break;
        }
        default:
            break;
    }
}

- (void)shareRecipe:(Recipe*)recipe
{
    NSString *space = @"\n\n";
    NSMutableString *title = [NSMutableString stringWithString:[recipe valueForKey:@"title"]];
    NSString *ingredients = [recipe valueForKey:@"ingredients"];
    NSString *howTo = [recipe valueForKey:@"howTo"];
    NSData *picture = [recipe valueForKey:@"picture"];
    
    [title appendString:space];
    [title appendString:ingredients];
    [title appendString:space];
    [title appendString:howTo];
    
    NSArray *sharingItems = [NSArray arrayWithObjects:title,picture,nil];
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
    
}

- (void)removeRecipe:(Recipe*)selectedRecipe inCell:(SWTableViewCell*)cell
{
    // Delete button was pressed
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    
    // Delete object from database
    [context deleteObject:selectedRecipe];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't delete! %@ %@", error, [error localizedDescription]);
        [AppDelegate displayUserErrorMessage:error];
    }
    else
    {
        // Remove recipe from table view
        
        if (self.searchDisplayController.active)
        {
            [searchResults removeObject:selectedRecipe];
            [recipesArray removeObject:selectedRecipe];
            
            [self.searchDisplayController.searchResultsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[self.searchDisplayController.searchResultsTableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationFade];
        }
        else
        {
            [recipesArray removeObject:selectedRecipe];
            
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:cellIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}

#pragma mark - Prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.tabBarController.tabBar setHidden:YES];
    
    if ([segue.identifier isEqualToString:@"showDetails"])
    {
        WriteRecipeController *destViewController = segue.destinationViewController;
        destViewController.segueIdentifier = segue.identifier;
        
        NSIndexPath *indexPath = nil;
        Recipe *selectedRecipe;
        
        if (self.searchDisplayController.active)
        {
            indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            selectedRecipe = [searchResults objectAtIndex:[indexPath row]];
        }
        else
        {
            indexPath = [self.tableView indexPathForSelectedRow];
            selectedRecipe = [recipesArray objectAtIndex:[indexPath row]];
        }
        destViewController.recipe = selectedRecipe;
    }
    else if ([segue.identifier isEqualToString:@"createRecipe"])
    {
        WriteRecipeController *destViewController = segue.destinationViewController;
        destViewController.segueIdentifier = segue.identifier;
        destViewController.del=self; // protocol listener
        
        [destViewController.editButton setTitle:@"Save"];
    }
    else if ([segue.identifier isEqualToString:@"copyPasteRecipe"])
    {
        WriteRecipeController *destViewController = segue.destinationViewController;
        destViewController.segueIdentifier = segue.identifier;
        destViewController.del=self; // protocol listener
    }
    else if([segue.identifier isEqualToString:@"pushWebView"])
    {
        NSData *myData = [self getDataFromFile:[self getRTFFileName]];
        
        DisplayRTFViewController *displayRTF = segue.destinationViewController;
        displayRTF.data = myData;
    }
}

# pragma mark - action sheet delegate methods

- (IBAction)openActionSheet:(id)sender
{
    if (SYSTEM_VERSION_LESS_THAN(@"8.0"))
    {
        UIActionSheet *actionsheet = [[UIActionSheet alloc]initWithTitle:@"New Recipe" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Create", @"Browse the web", nil];
        actionsheet.tag = 1;
        [actionsheet showInView:self.view];
    }
    else
    {
        
        UIAlertAction *create = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Create", @"Create")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                                            [self performSegueWithIdentifier:@"createRecipe" sender:self];
                                     }];
        
        UIAlertAction *browse = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"Browse the web", @"Browse the web")
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction *action)
                                      {
                                          [self performSegueWithIdentifier:@"copyPasteRecipe" sender:self];
                                      }];
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"New recipe"
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        [alertController addAction:create];
        [alertController addAction:browse];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
                [self performSegueWithIdentifier:@"createRecipe" sender:self];
                break;
                
            case 1:
                [self performSegueWithIdentifier:@"copyPasteRecipe" sender:self];
                break;
                
            default:
                break;
        }
    }
    else if(actionSheet.tag == 2)
    {
        switch (buttonIndex)
        {
            case 0:
                [self openRTFFile];
                break;
                
            case 1:
                [self shareViaEmail];
                break;
            default:
                break;
        }
    }
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    // Iterate through the sub views of the action sheet
    for (id actionSheetSubview in actionSheet.subviews) {
        // Change the font color if the sub view is a UIButton
        if ([actionSheetSubview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)actionSheetSubview;
            [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
            [button setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
        }
    }
}

#pragma mark - Favourite button

- (IBAction)displayFavourite:(id)sender
{
    UIImage *redHeartImage = [UIImage imageNamed:@"red_heart.png"];
    [recipesArray removeAllObjects];
    
    if(favs == 0)
    {
        NSLog(@"Show favourite recipes");
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"empty_heart.png"]];
        
        NSError *error = nil;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Recipe"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY favourite == %@", [NSNumber numberWithBool:YES]];
        [request setPredicate:predicate];
        recipesArray = [NSMutableArray arrayWithArray:[context executeFetchRequest:request error:&error]];
        favs = 1;
    }
    else
    {
        NSLog(@"Show all recipes");
        [self.navigationItem.rightBarButtonItem setImage:redHeartImage];
        
        [self fetchAllRecipes];
        favs = 0;
    }
    
    [self.tableView reloadData];
}

# pragma mark - Core Data

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *managedObjectContext = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        managedObjectContext = [delegate managedObjectContext];
    }
    return managedObjectContext;
}

- (void)fetchAllRecipes
{
    // Fetch the recipes from persistent data store
    NSError* error= nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Recipe"];
    recipesArray = [[context executeFetchRequest:fetchRequest error:&error] mutableCopy];
}

# pragma mark - Create rtf methods

- (void)createRTFWithContent:(NSString*)content
{
    NSString* fileName = @"Recipes.rtf";
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* rtfFileName = [path stringByAppendingPathComponent:fileName];
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:content attributes:nil];
    NSData *data = [str dataFromRange:(NSRange){0, [str length]} documentAttributes:@{NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType} error:NULL];
    [data writeToFile:rtfFileName atomically:YES];}

- (NSString*)getRTFFileName
{
    NSString* fileName = @"Recipes.rtf";
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* rtfFileName = [path stringByAppendingPathComponent:fileName];
    
    return rtfFileName;
    
}

- (NSString *)getAllRecipes
{
    NSMutableString *recipes = [NSMutableString new];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Recipe"];
    
    NSError *error = nil;
    
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    if (error != nil)
    {
        
        NSLog(@"Error while getting recipes from db!");
        return nil;
    }
    else
    {
        for(int i=0; i<[results count]; i++)
        {
            NSString *portions;
            NSString *duration;
            
            if([results[i] valueForKey:@"portions"])
                portions = [results[i] valueForKey:@"portions"];
            
            if([results[i] valueForKey:@"time"])
                duration = [results[i] valueForKey:@"time"];
            
            [recipes appendString:[results[i] valueForKey:@"title"]];
            [recipes appendString:@"\n\n"];
            
            if(duration)
            {
               [recipes appendString:[NSString stringWithFormat:@"Duration: %@\n",duration]];
            }
            
            if(portions)
            {
                [recipes appendString:[NSString stringWithFormat:@"Servers: %@",portions]];
                [recipes appendString:@"\n\n"];
            }
            
            [recipes appendString:[NSString stringWithFormat:@"Ingredients:\n%@",[results[i] valueForKey:@"ingredients"]]];
            [recipes appendString:@"\n\n"];
            [recipes appendString:[NSString stringWithFormat:@"Instructions:\n%@",[results[i] valueForKey:@"howTo"]]];
            
            [recipes appendString:@"\n\n\n\n\n"];
        }
        return recipes;
    }
}

- (NSData *)getDataFromFile:(NSString*)path
{
    NSError *error = nil;
    NSString *escapedUrlString = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL fileURLWithPath:escapedUrlString];
    
    NSData *rtfData = [NSData dataWithContentsOfURL:url options:NSDataReadingMappedAlways error:&error];
    if(error)
        [AppDelegate displayUserErrorMessage:error];
    
    return rtfData;
}

-(void)shareViaEmail
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    // Attach file to the email
    NSData *myData = [self getDataFromFile:[self getRTFFileName]];
    [picker addAttachmentData:myData mimeType:@"application/rtf" fileName:@"Recipes.rtf"];
    
    // Fill out the email body text
    NSString *emailBody = @"Recipes from Nyom Nyom app!";
    [picker setMessageBody:emailBody isHTML:NO];
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)openRTFFile
{
    [self performSegueWithIdentifier:@"pushWebView" sender:self];
}

#pragma mark - Mail delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Gesture Recognizer method

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    UIMenuController *menuCtrler = [UIMenuController sharedMenuController];
    
    if(menuCtrler.isMenuVisible)
        return NO;
    
    return YES;
}

#pragma mark - Shake gesture
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [self createRTFWithContent:[self getAllRecipes]];
            
            // update UI on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                if (SYSTEM_VERSION_LESS_THAN(@"8.0"))
                {
                    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"All recipes in text format"
                                                                             delegate:self
                                                                    cancelButtonTitle:@"Cancel"
                                                               destructiveButtonTitle:nil
                                                                    otherButtonTitles:@"Open file", @"Send via Email", nil];
                    actionSheet.tag = 2;
                    
                    [actionSheet showInView:self.view];
                }
                else
                {
                    
                    UIAlertAction *openRTF = [UIAlertAction
                                               actionWithTitle:@"Open file"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action)
                                               {
                                                   [self openRTFFile];
                                               }];
                    
                    UIAlertAction *viaEmail = [UIAlertAction
                                                 actionWithTitle:@"Send via Email"
                                                 style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action)
                                                 {
                                                     [self shareViaEmail];
                                                 }];
                    
                    UIAlertAction *cancelAction = [UIAlertAction
                                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                                   style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action)
                                                   {
                                                       NSLog(@"Cancel action");
                                                   }];
                    
                    
                    UIAlertController *alertController = [UIAlertController
                                                          alertControllerWithTitle:@"All recipes in text format"
                                                          message:nil
                                                          preferredStyle:UIAlertControllerStyleActionSheet];
                    [alertController addAction:openRTF];
                    [alertController addAction:viaEmail];
                    [alertController addAction:cancelAction];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            });
            
        });
    }
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

+ (BOOL)insertRecipesInContext:(NSManagedObjectContext*)context
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Recipe" inManagedObjectContext:context];
    
    // MUSSELS
    Recipe *mussels = [[Recipe alloc]initWithEntity:entity insertIntoManagedObjectContext:context];
    mussels.title = @"Mussels in White Wine and Garlic";
    mussels.ingredients = @"- 4 lbs live mussels \n- 2 cups dry white wine \n- 4 large shallots, finely chopped \n- 4 garlic cloves, finely chopped \n- 1/2 teaspoon salt \n- 1/3 cup mixed fresh herbs (such as flat-leaf parsley,chervil,or basil,chopped) \n- 6 tablespoons butter, cut into pieces";
    mussels.howTo = @"1. Rinse and scrub mussels under cold water. \n\n2. Using your fingers or paring knife, remove beards (strings that hang from the mussel shells), and discard. \n\n3. In a large stockpot set over medium heat, combine wine, shallots, garlic, and salt. \n\n4. Simmer 5 minutes. \n\n5. Add mussels, cover, and increase heat to high. \n\n6. Cook until all mussels are open, about 5 minutes. \n\n7. Stir in herbs and butter. \n\n8. Remove from heat. \n\n9. Divide mussels and broth among four bowls. \n\n10. Serve immediately. \n\n Source: www.food.com";
    NSString *str = [[NSBundle mainBundle] pathForResource:@"mussels" ofType:@"png"];
    
    mussels.picture = [NSData dataWithContentsOfFile:str];
    mussels.time = @"25 mins";
    mussels.portions = @"4 serves";
    
    // CARBONARA PASTA
    
    Recipe *pasta = [[Recipe alloc]initWithEntity:entity insertIntoManagedObjectContext:context];
    pasta.title = @"Creamy courgette & bacon pasta";
    pasta.ingredients = @"- 1 tsp olive oil \n- 150g diced pancetta or smoked bacon lardons \n- 4 courgettes, coarsely grated, \n- 1 garlic clove, crushed \n- handful freshly grated parmesan \n- 1 small tub (200g) low-fat crème fraîche \n- 300g tagliatelle";
    pasta.howTo = @"1. Heat the olive oil in a large frying pan and sizzle the pancetta or bacon for about 5 mins until starting to crisp. Turn up the heat and add the grated courgette to the pan. Cook for 5 mins or until soft and starting to brown then add the garlic and cook for a minute longer. Season and set aside. \n\n2. Cook the tagliatelle according to the pack instructions and scoop out a cupful of cooking water. Drain the tagliatelle and tip into the frying pan with the bacon and courgette. Over a low heat toss everything together with the crème fraiche and half the Parmesan adding a splash of pasta water too if you need to loosen the sauce. Season to taste and serve twirled into bowls with the remaining Parmesan scattered over. \n\nSource: www.bbcgoodfood.com";
    str = [[NSBundle mainBundle] pathForResource:@"pasta" ofType:@"png"];
    pasta.picture = [NSData dataWithContentsOfFile:str];
    pasta.time = @"30 mins";
    pasta.portions = @"4 serves";
    
    
    // STEAK
    
    Recipe *steak = [[Recipe alloc]initWithEntity:entity insertIntoManagedObjectContext:context];
    steak.title = @"Tea-rubbed rib of beef with heirloom carrots";
    steak.ingredients = @"FOR THE BEEF: \n- 4 lapsang souchong tea bags \n- 1 tablespoon light brown muscovado sugar \n- 2 teaspoons Maldon sea salt \n- 1 teaspoon Chinese 5-spice powder \n- 1/2 teaspoon freshly ground black pepper \n- 2.5kg rib of beef \n- 2 tablespoons rapeseed oil \n- 400g elephant shallots, halved lengthways \n- 45ml dry sherry \n- 500ml beef stock \n- 150ml vegetable stock \n- 1 tablespoon soy sauce \n FOR THE HEIRLOOM CARROTS: \n- 900g heirloom carrots, peeled and sliced on the diagonal \n- 2 tablespoons caster sugar \n- 25g butter \n- 1 teaspoon cumin seeds, lightly toasted \n- 1 tablespoon finely chopped flat-leaf parsley";
    steak.howTo = @"1. For the beef, tear open the tea bags and pour the contents into a mixing bowl. Stir in the sugar, salt, 5-spice powder and pepper. Rub the meat all over with the oil, then massage the tea rub into the meat. Leave for 2-3 hours in the fridge to allow the flavours to develop. \n\n2. Take the meat out of the fridge 15 minutes before starting to cook. Heat the oven to 220°C/ fan oven 200°C/mark 7. Place the shallots in a roasting tin and place the rib of beef on top. Roast in the oven for 30 minutes, then turn the heat down to 190°C/fan oven 170°C/mark 5 and continue to cook for 65-70 minutes for medium rare. If you have a meat thermometer, it should read 65°C in the centre of the meat. Lift the meat out of the roasting tin and leave to rest, covered in aluminium foil, for 15 minutes. Place the shallots in a saucepan, removing any very burnt ones, add the sherry and cook for a few minutes over a high heat until the alcohol has almost evaporated. Add both stocks, turn down the heat and simmer for 5 minutes. Add the soy sauce. Pass through a sieve and discard the shallots. Serve the meat in slices with the juices. \n\n3. Five minutes before you take the beef out of the oven, place the carrots in a large saucepan and pour in just enough cold water to cover by 5mm. Add the sugar, butter and a pinch of salt and bring to the boil, uncovered. Boil for 15 minutes, by which time nearly all the liquid should have evaporated. Drain if necessary. Stir in the cumin seeds and parsley and serve with the beef. \n\nTo drink: The beef is versatile, but the sweetness of the carrots can overwhelm very dry reds, such as traditional Bordeaux, so choose a fruitier style such as New Zealand Syrah or Pinot Noir, Châteauneuf-du-Pape, Garnacha, Primitivo, or this Tuscan red: Scopetone Sangiovese di Toscana 2010, £12.95, Berry Bros & Rudd. Wine details correct at original magazine publication date. \n\nSource: www.houseandgarden.co.uk" ;
    steak.portions = @"6 serves";
    steak.time = @"unknown";
    str = [[NSBundle mainBundle] pathForResource:@"steak" ofType:@"png"];
    steak.picture = [NSData dataWithContentsOfFile:str];
    
    // SEA BASS
    
    Recipe *seaBass = [[Recipe alloc]initWithEntity:entity insertIntoManagedObjectContext:context];
    seaBass.title = @"Baked Sea Bass with Baby Leeks, Potatoes, Raki and Dill";
    seaBass.ingredients = @"- 500g waxy, salad or baby new potatoes \n- Zest of 1 lemon, finely grated \n- Generous bunch dill, very roughly chopped \n- 4 tablespoons extra-virgin olive oil, plus extra for drizzling \n- 1 sea bass, about 2.5kg, cleaned and scaled \n- 100ml raki (or pastis) \n- 350g baby leeks \nTO SERVE: \n- Roast tomatoes (optional)";
    seaBass.howTo = @"1. Heat the oven to 200°C/fan oven 180°C/mark 6. Cut the potatoes in slices about the thickness of a pound coin (or thinner if cooking two smaller fish). There's no need to peel them. Spread them on the base of a roasting tin, with the lemon zest and half the dill. Season with salt and pepper and add 2 tablespoons olive oil. Turn them with your hands to coat. \n\n2. Make 3 slits on each side of the fish. Rub with the remaining 2 tablespoons oil, push half the remaining dill into the slits and season inside and out. Place on the bed of potatoes and put remaining dill into the belly. Pour the raki over the fish and potatoes. Cook in the oven for 30 minutes (or about 22 minutes for 2 smaller fish). Check for doneness - the flesh near the bone at the thickest part of the fish should be opaque. \n\n3. About 5 minutes before the fish will be ready, steam the baby leeks until tender - about 4 minutes. Drizzle them with oil and season. \n\n4. Move the fish and potatoes carefully to a heated platter, surround with the leeks and serve immediately. \n\nTo drink: A dry white with a grassy/herby accent to echo the raki and dill, such as Sauvignon Blanc, Picpoul, Chablis (unoaked), Grüner Veltliner, Falanghina or Grechetto: Colli Martani Grechetto 2012, £11.95, Jeroboams. Wine details correct at original magazine publication date. \n\nSource: www.houseandgarden.co.uk";
    str = [[NSBundle mainBundle] pathForResource:@"seaBass" ofType:@"png"];
    seaBass.picture = [NSData dataWithContentsOfFile:str];
    seaBass.portions = @"6 serves";
    seaBass.time = @"unknown";
    
    // LEMON CAKE
    Recipe *lemonCake = [[Recipe alloc]initWithEntity:entity insertIntoManagedObjectContext:context];
    lemonCake.title = @"Iced-Lemon Strawberry Cake";
    lemonCake.ingredients = @"FOR THE SPONGE: \n- Sunflower oil, for greasing \n- 85g caster sugar, plus extra for dusting \n- 85g plain flour, sifted, plus extra for dusting \n- A pinch of salt \n- 3 medium eggs \n-Finely grated zest of 1 lemon \nFOR THE STRAWBERRIES:\n- 450g ripe strawberries, hulled and finely sliced \n- 3 tablespoons St-Germain elderflower liqueur \n- 2 tablespoons icing sugar (or to taste) \nFOR THE LEMON ICING: \n- 170g sifted fondant icing sugar \n- 3 tablespoons lemon juice \nTO DECORATE: \n- A few curls of lemon rind (optional)";
    lemonCake.howTo = @"1. Heat the oven to 180°C/mark 4/fan oven 160°C. Lightly oil 2 non-stick, 18cm-diameter Victoria sponge cake tins. Line the base of each with baking paper, brush again with oil and dust with sugar, then flour. \n\n2. Sift the flour and salt into a bowl. Place the eggs and sugar in another large bowl.  If you have an electric whisk, beat until the mixture is pale and thick and leaves a trail when you remove the whisk. If doing it by hand, whisk over a pan of just simmered water and, once it reaches the right consistency, remove from the heat and continue to whisk until cool. \n\n3. Using a metal spoon, gently fold the flour and lemon zest into the mixture. Divide between the cake tins. Bake in the centre of the oven for 15 minutes, or until golden and slightly shrunk from the sides of the tin. Test by lightly pressing the cake with your fingertips.  The mix will spring back leaving no indentation if cooked. Cool in the tin on a wire rack. After 5 minutes remove from the tin. Leave until cold. Once cold, peel off the paper. \n\n4. While the cake is cooking, macerate the strawberries. Place in a bowl and mix in the liqueur and icing sugar to taste. Remember that the cake will be quite sweet once iced. \n\n5. Place the first sponge, bottom side up, on a serving plate. Spoon the strawberries and their juice on to the sponge. Gently press the other sponge, baked side up on top of the strawberries. \n\n6. For the lemon icing, sift the icing sugar into a small bowl. Using a wooden spoon, mix in the lemon juice so that it forms a thick icing. Spoon over the top of the cake and let it slowly drizzle down the sides.  Leave until set - this will take a couple of hours - then decorate with lemon rind, if using. \n\nTo drink: A sweet white with good acidity. Sparkling Moscato works well, but if you prefer something still, this New Zealand wine is ideal:The Ned Noble Sauvignon 2010, £12.99 for 37.5cl, from Majestic. Wine details correct at original magazine publication date. \n\nSource: www.houseandgarden.co.uk";
    lemonCake.portions = @"6 serves";
    lemonCake.time = @"unknown";
    str = [[NSBundle mainBundle] pathForResource:@"lemonCake" ofType:@"png"];
    lemonCake.picture = [NSData dataWithContentsOfFile:str];
    
    // MERINGUE
    
    Recipe *meringue = [[Recipe alloc]initWithEntity:entity insertIntoManagedObjectContext:context];
    meringue.title = @"Soft Walnut Meringue with Blackberries";
    meringue.ingredients = @"FOR THE MERINGUE: \n- 50g walnut kernels \n- 6 medium egg whites \n- 250g caster sugar \n- 1 1/2 teaspoons cornflour \n- 1 1/2 tablespoons white-wine vinegar \n- Icing sugar, for dusting \nFOR THE TOPPING: \n- 285ml double cream \n- 1 tablespoon apple brandy (optional) \n- 500g blackberries \n- 1-2 tablespoons icing sugar";
    meringue.howTo = @"1. Heat the oven to 150°C/fan oven 130°C/mark 2. Line a baking sheet with baking paper. Place the walnut kernels in a food processor or blender and whizz in short bursts, until they're finely ground. \n\n2. Put the egg whites in a large, clean, dry bowl. Whisk until they form soft peaks. Whisk in a third of the sugar and as soon as the egg whites begin to look more glossy, whisk in another third, fol­lowed by the remaining third. It will only take a few minutes. When the meringue is really thick and glossy, fold in the cornflour, vinegar and ground walnuts with a flat metal spoon. \n\n3. Spread the mixture evenly over the baking paper in a 20 x 30cm rectangle. Bake for 20-25 minutes or until soft and marshmallow­ like. Leave to rest on the baking sheet for a few minutes. Liberally dust a sheet of baking paper with sifted icing sugar. Carefully invert the meringue on to the sugar­ dusted paper and gently peel away the baking paper it was cooked on. \n\n4. Shortly before serving, spread the meringue with either plain whipped cream or apple-brandy cream. For the latter, add the apple brandy to the cream in a large bowl and then whisk until the cream forms soft peaks. Spoon over the meringue, then either cut it into 6 squares, place on serving plates and top each with blackberries and a sifting of icing sugar, or scatter with blackberries and sift with icing sugar before cutting. \n\nTo drink: Meringue is always very sweet, but the walnuts soften and enrich the flavour. You could drink a Moscato, as above, or try a young Sauternes or Monbazillac: Domaine de L'Ancienne Cure Monbazillac 2010, 8.75 for 37.5cl, Yapp Brothers (01747 860423). \n\nSource: www.houseandgarden.co.uk";
    meringue.portions = @"6 serves";
    meringue.time = @"unknown";
    str = [[NSBundle mainBundle] pathForResource:@"meringue" ofType:@"png"];
    meringue.picture = [NSData dataWithContentsOfFile:str];
    
    // COD
    
    Recipe *cod = [[Recipe alloc]initWithEntity:entity insertIntoManagedObjectContext:context
    ];
    cod.title = @"Tomato & thyme cod";
    cod.ingredients = @"- 1 tbsp olive oil \n- 1 onion, chopped \n- 400g can chopped tomatoes \n- 1 heaped tsp light, soft brown sugar \n- few sprigs thyme, leaves stripped \n- 1 tbsp soy sauce \n- 4 cod, fillets (we like No Catch) or another white flaky fish, such as pollack";
    cod.howTo = @"1. Heat the oil in a frying pan, add the onion, then fry for 5-8 mins until lightly browned. Stir in the tomatoes, sugar, thyme and soy, then bring to the boil. \n\n2. Simmer 5 mins, then slip the cod into the sauce. Cover and gently cook for 8-10 mins until the cod flakes easily. Serve with baked or steamed potatoes. \n\nSource: www.bbcgoodfood.com";
    cod.portions = @"4 serves";
    cod.time = @"20 mins";
    str = [[NSBundle mainBundle] pathForResource:@"cod" ofType:@"png"];
    cod.picture = [NSData dataWithContentsOfFile:str];
    
    // Meatballs
    
    Recipe *meatballs = [[Recipe alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    meatballs.title = @"Speedy Moroccan meatballs";
    meatballs.ingredients = @"- 1 tbsp olive oil \n- 350g pack ready-made beef or chicken meatballs (approx 16) \n- 1 large onion, sliced \n- 100g dried apricots, halved \n- 1 small cinnamon stick \n- 400g tin chopped tomatoes with garlic \n- 25g toasted flaked almonds \n- handful coriander, roughly chopped";
    meatballs.howTo = @"1. Heat the oil in a large deep frying pan, then fry the meatballs for 10 mins, turning occasionally until cooked through. Scoop out of the pan and set aside, then cook the onion for 5 mins, until softened. \n\n2. Add the dried apricots, cinnamon stick, tomatoes and half a can of water to the pan, then bring to the boil and simmer for 10 mins. Remove the cinnamon stick. Return the meatballs to the pan and coat well with the tomato sauce. Serve sprinkled with the almonds and coriander. \n\nSource: www.bbcgoodfood.com";
    meatballs.time = @"20 mins";
    meatballs.portions = @"4 serves";
    
    str = [[NSBundle mainBundle] pathForResource:@"meatballs" ofType:@"png"];
    meatballs.picture = [NSData dataWithContentsOfFile:str];
    
    NSError *error = nil;
    if (![context save:&error])
    {
        NSLog(@"Can't save default recipes!");
        NSString *valueToSave = @"NO";
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"recipes"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return NO;
    }
    else
    {
        // Mark the fact that recipes are already in database
        
        NSString *valueToSave = @"YES";
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"recipes"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return YES;
    }
}

#pragma mark - getting the new created recipe

-(void)sendRecipe:(Recipe *)recipe
{
    [recipesArray addObject:recipe];
}

@end
