//
//  WriteRecipeController.m
//  yummy
//
//  Created by Dalia on 11/08/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#define SYSTEM_VERSION_LESS_THAN(v)   ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#import "WriteRecipeController.h"
#import "AppDelegate.h"

@interface WriteRecipeController ()
{
    UIBarButtonItem *backwardButton;
    UIBarButtonItem *forwardButton;
    UIButton *savePictureButton;
    UIImagePickerController *imagePicker;
    UIImage *recipePicture;
    BOOL isFullScreen;
    CGRect prevFrame;
    int keyboardHeight;
    UIToolbar* keyboardDoneButtonView;
    UIPickerView *timePicker;
    NSMutableArray *hoursArray;
    NSMutableArray *minsArray;

}
@end

@implementation WriteRecipeController
@synthesize recipe, del;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.name.delegate = self;
    self.ingredientsTextView.delegate = self;
    self.howTo.delegate = self;
    self.portions.delegate = self;
    self.time.delegate = self;
    self.webView.delegate = self;
    self.tapGesture.delegate = self;
    self.automaticallyAdjustsScrollViewInsets = NO;

    [self.ingredientsTextView.layer setBorderColor:[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2] CGColor]];
    [self.ingredientsTextView.layer setCornerRadius:5];
    [self.ingredientsTextView.layer setBorderWidth:0.5];
    
    [self.howTo.layer setBorderColor:[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2] CGColor]];
    [self.howTo.layer setCornerRadius:5];
    [self.howTo.layer setBorderWidth:0.5];
    
    // SAVE PICTURE BUTTON
    
    savePictureButton = [[UIButton alloc]initWithFrame:CGRectMake(20, [AppDelegate getScreenHeight]-30, 50, 20)];
    [savePictureButton setTintColor:[UIColor redColor]];
    [savePictureButton setTitle:@"Save" forState:UIControlStateNormal];
    [self.backgroundPicture addSubview:savePictureButton];
    
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    // Added DONE button to keyboard
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace, doneButton, nil]];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(1, 0)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    if ([self.segueIdentifier isEqualToString:@"showDetails"])
    {
        [self.name setTextColor:[UIColor blackColor]];
        [self.ingredientsTextView setTextColor:[UIColor blackColor]];
        [self.howTo setTextColor:[UIColor blackColor]];
        
        if([[self.recipe valueForKey:@"portions"] isEqualToString:@"Add portions"])
            [self.portions setTextColor:[UIColor lightGrayColor]];
        else
            [self.portions setTextColor:[UIColor blackColor]];
        
        if([[self.recipe valueForKey:@"time"] isEqualToString:@"Add time"])
            [self.time setTextColor:[UIColor lightGrayColor]];
        else
            [self.time setTextColor:[UIColor blackColor]];
        
        [self makeControlsNonEditable];
        
         prevFrame.origin.y += self.navigationController.navigationBar.frame.size.height + 20;
    }
    else if([self.segueIdentifier isEqualToString:@"copyPasteRecipe"])
    {
        [self.panGesture setEnabled:YES];
        [self.webViewDetails setHidden:NO];
        [self.upButton setHidden:NO];
        
        forwardButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"forward.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(moveToPageForward)];
        [forwardButton setEnabled:NO];
        
        backwardButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"backwards.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(moveToPageBackward)];
        [backwardButton setEnabled:NO];
        
        UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveRecipe:)];
        
        [self.navigationItem setRightBarButtonItems:@[saveButton, flexibleSpace, forwardButton, flexibleSpace, backwardButton, flexibleSpace]];
        
        [self.goButton setHidden:NO];
        [self.downButton setHidden:NO];
        [self.address setHidden:NO];
        [self.webView setHidden:NO];
        [self makeControlsEditable];
        
        [self go:self.goButton];
        
    }
    else
    {
        [self makeControlsEditable];
    }
    
    if ([self.navigationItem.rightBarButtonItem.title isEqualToString:@"Edit"])
        self.scrollView.scrollEnabled = NO;

    
    self.ingredientsTextView.delegate = self;
    self.howTo.delegate = self;
    self.name.delegate = self;
    self.portions.delegate = self;
    self.time.delegate = self;
    
    if(self.recipe)
    {
        [self.name setText:[self.recipe valueForKey:@"title"]];
        [self.portions setText:[self.recipe valueForKey:@"portions"]];
        [self.time setText:[self.recipe valueForKey:@"time"]];
        [self.ingredientsTextView setText:[self.recipe valueForKey:@"ingredients"]];
        [self.howTo setText:[self.recipe valueForKey:@"howTo"]];
        [self.picture setImage:[UIImage imageWithData:[self.recipe valueForKey:@"picture"]]];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Keyboard Notification

- (void)keyboardDidShow: (NSNotification *) notif
{
    CGSize keyboardSize = [[[notif userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    
    if(!([self.segueIdentifier isEqualToString:@"showDetails"] && [self.navigationItem.rightBarButtonItem.title isEqualToString:@"Edit"]))
    {
        self.scrollView.scrollEnabled = YES;
        [self.panGesture setEnabled:NO];
        [self.upButton setHidden:YES];
        
        CGSize scrollViewContentSize;
        
        int height = [AppDelegate getScreenHeight];
        int width = [AppDelegate getScreenWidth];
        
        if (width == 320)
        {
            if(height == 480)
            {
                if(keyboardHeight <= 265) // iphone 4/4S ios7
                    scrollViewContentSize = CGSizeMake(320, 727);
                
                else // iphone 4/4S ios8
                    scrollViewContentSize = CGSizeMake(320, 760);
            }
            else if(height == 568)
            {
                if(keyboardHeight <= 265) // iphone 5/5S ios7
                    scrollViewContentSize = CGSizeMake(320, 812);
                
                else // iphone 5/5S ios8
                    scrollViewContentSize = CGSizeMake(320, 850);
            }
        }
        else
        {
            if (height == 667) // iphone 6
                scrollViewContentSize = CGSizeMake(375, 955);
            
            else if (height == 736) //iphone 6plus
                scrollViewContentSize = CGSizeMake(414, 1035);
        }
        
        [self.scrollView setContentSize:scrollViewContentSize];
    }
}

- (void)keyboardDidHide: (NSNotification *) notif
{
    if(!([self.segueIdentifier isEqualToString:@"showDetails"] && [self.navigationItem.rightBarButtonItem.title isEqualToString:@"Edit"]))
    {
        self.scrollView.scrollEnabled = NO;
        [self.panGesture setEnabled:YES];
    }
    
//    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
//    self.scrollView.contentInset = contentInsets;
//    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark -

- (void)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
    [self.scrollView scrollRectToVisible:self.name.frame animated:YES];
    
    if([self.segueIdentifier isEqualToString:@"copyPasteRecipe"])
        [self.upButton setHidden:NO];
}

- (IBAction)go:(id)sender
{
    NSMutableString *currentLink = [NSMutableString stringWithString:self.address.text];
    if([currentLink rangeOfString:@"http"].location == NSNotFound)
        [currentLink insertString:@"https://" atIndex:0];
    
    if([currentLink rangeOfString:@"www."].location == NSNotFound)
        [currentLink insertString:@"www." atIndex:8];
    
    self.address.text = currentLink;
    
    NSURL *url = [NSURL URLWithString:currentLink];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    
    [self.address resignFirstResponder];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.backgroundActivityIndicator setHidden:NO];
    [self.activityIndicator startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"%@",webView.request.mainDocumentURL);
    self.address.text = [webView.request.mainDocumentURL absoluteString];
    [self.activityIndicator stopAnimating];
    [self.backgroundActivityIndicator setHidden:YES];
    
    [self enableDisableButtons];
}

- (void)enableDisableButtons
{
    // Enable or disable back
    if ([self.webView canGoBack])
    {
        [backwardButton setEnabled:YES];
    } else
    {
        [backwardButton setEnabled:NO];
    }
    
    // Enable or disable forward
    if ([self.webView canGoForward])
    {
        [forwardButton setEnabled:YES];
    }
    else
    {
        [forwardButton setEnabled:NO];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if(error.code != -999)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"An error occured..." message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)moveToPageForward
{
    [self.webView goForward];
}

- (void)moveToPageBackward
{
    [self.webView goBack];
}
- (IBAction)panningDown:(id)sender
{
    CGPoint vel = [sender velocityInView:self.view];
    CGFloat screenHeight = [AppDelegate getScreenHeight];
    
    if(![self.activityIndicator isAnimating])
    {
        if (vel.y > 0)
        {
            if (![self.downButton isHidden])
            {
                [UIView animateWithDuration:0.5 animations:^{
                    NSLog(@"%f", self.webViewDetails.frame.origin.y);
                    [self.webViewDetails setFrame:CGRectMake(self.webViewDetails.frame.origin.x, screenHeight, self.webViewDetails.frame.size.width, self.webViewDetails.frame.size.height)];
                    
                    [self.downButton setHidden:YES];
                    [self.upButton setHidden:NO];
                    [backwardButton setEnabled:NO];
                    [forwardButton setEnabled:NO];
                } completion:^(BOOL finished){
                [self.webViewDetails setHidden:YES];
                }];
            }
        }
        else
        {
            if ([self.downButton isHidden])
            {
                [UIView animateWithDuration:0.5 animations:^{
                    NSLog(@"%f", self.webViewDetails.frame.origin.y);
                    [self.webViewDetails setFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, self.webViewDetails.frame.size.width, self.webViewDetails.frame.size.height)];
                    
                }];
                [self.webViewDetails setHidden:NO];
                [self.downButton setHidden:NO];
                [self.upButton setHidden:YES];
                [self enableDisableButtons];
            }
        }
    }
}

- (void)makeControlsEditable
{
    [self.name setEditable:YES];
    [self.portions setUserInteractionEnabled:YES];
    [self.time setUserInteractionEnabled:YES];
    [self.ingredientsTextView setEditable:YES];
    [self.howTo setEditable:YES];
    
    self.name.inputAccessoryView = keyboardDoneButtonView;
    self.ingredientsTextView.inputAccessoryView = keyboardDoneButtonView;
    self.howTo.inputAccessoryView = keyboardDoneButtonView;
    self.portions.inputAccessoryView = keyboardDoneButtonView;
    self.time.inputAccessoryView = keyboardDoneButtonView;
}

- (void)makeControlsNonEditable
{
    [self.name setEditable:NO];
    [self.portions setUserInteractionEnabled:NO];
    [self.time setUserInteractionEnabled:NO];
    [self.ingredientsTextView setEditable:NO];
    [self.howTo setEditable:NO];
    
    self.name.inputAccessoryView = nil;
    self.ingredientsTextView.inputAccessoryView = nil;
    self.howTo.inputAccessoryView = nil;
    self.portions.inputAccessoryView = nil;
    self.time.inputAccessoryView = nil;
}

- (BOOL)emptyTextViews
{
    if([self.name.text isEqualToString:@""] ||
       [self.name.text isEqualToString:@"Add title"] ||
       [self.ingredientsTextView.text isEqualToString:@""] ||
       [self.ingredientsTextView.text isEqualToString:@"Add ingredients"] ||
       [self.howTo.text isEqualToString:@""] ||
       [self.howTo.text isEqualToString:@"Add instructions"])
        return YES;
    return NO;
}

- (BOOL)saveRecipe:(id)sender
{
    if(![self emptyTextViews])
    {
        NSString *msg;
        Recipe *newRecipe;
        NSManagedObjectContext *context = [self managedObjectContext];
        
        NSNumberFormatter * format = [[NSNumberFormatter alloc] init];
        [format setNumberStyle:NSNumberFormatterDecimalStyle];
        
        if(self.recipe)
        {
            // Update existing recipe
           if(![[self.recipe valueForKey:@"title"] isEqualToString:self.name.text])
           {
                [self.recipe setValue:self.name.text forKey:@"title"];
                msg = @"Recipe updated!";
           }
        
            if(![[self.recipe valueForKey:@"portions"] isEqualToString:self.portions.text] && ![self.portions.text isEqualToString:@"Add portions"] && ![self.portions.text isEqualToString:@""])
            {
                [self.recipe setValue:self.portions.text forKey:@"portions"];
                msg = @"Recipe updated!";
            }
            
            if(![[self.recipe valueForKey:@"time"] isEqualToString:self.time.text] && ![self.time.text isEqualToString:@"Add time"] && ![self.time.text isEqualToString:@""])
            {
                [self.recipe setValue:self.time.text forKey:@"time"];
                msg = @"Recipe updated!";
            }
            
            if(![[self.recipe valueForKey:@"ingredients"] isEqualToString:self.ingredientsTextView.text])
            {
                [self.recipe setValue:self.ingredientsTextView.text forKey:@"ingredients"];
                msg = @"Recipe updated!";
            }
           
            if(![[self.recipe valueForKey:@"howTo"] isEqualToString:self.howTo.text])
            {
                [self.recipe setValue:self.howTo.text forKey:@"howTo"];
                msg = @"Recipe updated!";
            }
            
            if(recipePicture != nil)
            {
                [self.recipe setValue:UIImagePNGRepresentation(recipePicture) forKey:@"picture"];
                [self.picture setImage:recipePicture];
                msg = @"Recipe updated!";
            }
        }
        else
        {
            // Create a new recipe
            newRecipe = [NSEntityDescription insertNewObjectForEntityForName:@"Recipe" inManagedObjectContext:context];
            [newRecipe setValue:self.name.text forKey:@"title"];
            
            [newRecipe setValue:self.portions.text forKey:@"portions"];
            [newRecipe setValue:self.time.text forKey:@"time"];
            
            [newRecipe setValue:self.ingredientsTextView.text forKey:@"ingredients"];
            [newRecipe setValue:self.howTo.text forKey:@"howTo"];
            [newRecipe setValue:[NSNumber numberWithBool:NO] forKey:@"favourite"];
            if(recipePicture != nil)
            {
                [newRecipe setValue:UIImagePNGRepresentation(self.picture.image) forKey:@"picture"];
                [self.picture setImage:recipePicture];
            }
            msg = @"Recipe saved!";
        }
        
        [self.scrollView scrollRectToVisible:self.name.frame animated:YES];
        [self.name endEditing:YES];
        [self.portions endEditing:YES];
        [self.time endEditing:YES];
        [self.ingredientsTextView endEditing:YES];
        [self.howTo endEditing:YES];
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
            return NO;
        }
        else
        {
            if(msg !=nil)
            {
                if(newRecipe)
                {
                    [del sendRecipe:newRecipe];
                }
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Great" otherButtonTitles:nil, nil];
                [alert show];
                [self.navigationController popViewControllerAnimated:YES];
                return YES;
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info" message:@"Insert a name, ingredients and instructions for the recipe..." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    return NO;
}

- (IBAction)editButtonActions:(id)sender
{
    if([self.editButton.title isEqualToString:@"Edit"])
    {
        [self.editButton setTitle:@"Save"];
        [self makeControlsEditable];
    }
    else
    {
        if([self saveRecipe:nil])
        {
            [self.editButton setTitle:@"Edit"];
            [self makeControlsNonEditable];
        }
    }
}
- (void)layoutSubviewsForImage:(UIImageView *)imageView;
{
    // center the image as it becomes smaller than the size of the screen
    CGSize boundsSize = self.view.bounds.size;
    CGRect frameToCenter = [[UIScreen mainScreen] bounds];
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;//-self.navigationController.navigationBar.frame.size.height - 20;
    
    NSLog(@"%lf %lf %lf %lf", self.picture.frame.origin.x, self.picture.frame.origin.y, self.picture.frame.size.width, self.picture.frame.size.height);
    self.picture.translatesAutoresizingMaskIntoConstraints = YES;
    self.picture.frame = frameToCenter;
    NSLog(@"%lf %lf %lf %lf", self.picture.frame.origin.x, self.picture.frame.origin.y, self.picture.frame.size.width, self.picture.frame.size.height);
    
    
    [self.view addSubview:imageView];
}
-(void)fullScreenForImage:(UIImageView*)imageView
{
    if (!isFullScreen) {
        [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
            [self.view setBackgroundColor:[UIColor clearColor]];
            
            // save previous frame of picture
            prevFrame.origin.x = self.picture.frame.origin.x;
            prevFrame.origin.y += self.picture.frame.origin.y;
            prevFrame.size = self.picture.frame.size;
            
            self.ingredientsTextView.translatesAutoresizingMaskIntoConstraints = YES;
            imageView.translatesAutoresizingMaskIntoConstraints = YES;
            self.portions.translatesAutoresizingMaskIntoConstraints = YES;
            self.time.translatesAutoresizingMaskIntoConstraints = YES;
            [self layoutSubviewsForImage:imageView];
            [self.backgroundPicture setHidden:NO];
            //
        }completion:^(BOOL finished){
            isFullScreen = true;
        }];
        return;
    } else {
        [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
            [self.navigationController setNavigationBarHidden:NO animated:NO];
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
            [self.backgroundPicture setHidden:YES];
             [self.view setBackgroundColor:[UIColor whiteColor]];
            self.ingredientsTextView.translatesAutoresizingMaskIntoConstraints = YES;
            imageView.translatesAutoresizingMaskIntoConstraints = YES;
            self.portions.translatesAutoresizingMaskIntoConstraints = YES;
            self.time.translatesAutoresizingMaskIntoConstraints = YES;
            [imageView setFrame:prevFrame];
            prevFrame = CGRectMake(0, 0, 0, 0);
        }completion:^(BOOL finished){
            isFullScreen = false;
        }];
        return;
    }
}

#pragma mark - Tap gesture

-  (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([savePictureButton pointInside:[touch locationInView:savePictureButton] withEvent:nil])
    {
        [self forwardButtonTouched];
        return NO;
    }
    return YES;
}

- (IBAction)touchedPicture:(id)sender
{
    if([self.navigationItem.rightBarButtonItem.title isEqualToString:@"Edit"])
    {
        [self fullScreenForImage:self.picture];
    }
    else
    {
        if([self.segueIdentifier isEqualToString:@"createRecipe"])
        {
        imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setDelegate:self];
        
        if (SYSTEM_VERSION_LESS_THAN(@"8.0"))
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Set photo"
                                                                     delegate:self
                                                            cancelButtonTitle:@"Cancel"
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:@"Take photo", @"Choose photo", nil];
            actionSheet.tag = 1;
            
            [actionSheet showInView:self.view];
        }
        else
        {
            
            UIAlertAction *openCamera = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"Take photo", @"OpenCamera")
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action)
                                         {
                                             if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                                                 [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
                                                 
                                                 [self presentViewController:imagePicker animated:YES completion:nil];
                                             }
                                         }];
            
            UIAlertAction *openLibrary = [UIAlertAction
                                          actionWithTitle:NSLocalizedString(@"Choose photo", @"OpenLibrary")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction *action)
                                          {
                                              [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                              
                                              [self presentViewController:imagePicker animated:YES completion:nil];
                                          }];
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                           style:UIAlertActionStyleCancel
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"Cancel action");
                                           }];
            
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Set picture"
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            [alertController addAction:openCamera];
            [alertController addAction:openLibrary];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        }
        else
        {
            UIMenuController *menuController = [UIMenuController sharedMenuController];
            [menuController setTargetRect:CGRectMake(50.0, 50.0, 0, 0) inView:self.picture];
            [menuController setMenuVisible:YES animated:YES];
        }
    }
}

- (void)paste:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    UIImage *sImage = nil;
    if(pasteboard.string)
    {
        NSString *WEB_ARCHIVE = @"Apple Web Archive pasteboard type";
        if ([[[UIPasteboard generalPasteboard] pasteboardTypes] containsObject:WEB_ARCHIVE])
        {
            NSData* archiveData = [[UIPasteboard generalPasteboard] valueForPasteboardType:WEB_ARCHIVE];
            if (archiveData)
            {
                NSError* error = nil;
                id webArchive = [NSPropertyListSerialization propertyListWithData:archiveData options:NSPropertyListImmutable format:NULL error:&error];
                if (error) {
                    return;
                }
                NSArray *subItems = [NSArray arrayWithArray:[webArchive objectForKey:@"WebSubresources"]];
                NSPredicate *iPredicate = [NSPredicate predicateWithFormat:@"WebResourceMIMEType like 'image*'"];
                NSArray *imagesArray = [subItems filteredArrayUsingPredicate:iPredicate];
                for (int i=0; i<[imagesArray count]; i++) {
                    NSDictionary *sItem = [NSDictionary dictionaryWithDictionary:[imagesArray objectAtIndex:i]];
                    sImage = [UIImage imageWithData:[sItem valueForKey:@"WebResourceData"]];
                }
            }
        }
        else
        {
            if ([pasteboard.string rangeOfString:@"imgurl="].location != NSNotFound)
            {
                NSString *param = nil;
                NSRange start = [pasteboard.string rangeOfString:@"imgurl="];
                if (start.location != NSNotFound)
                {
                    param = [pasteboard.string substringFromIndex:start.location + start.length];
                    NSRange end = [param rangeOfString:@"&"];
                    if (end.location != NSNotFound)
                    {
                        param = [param substringToIndex:end.location];
                        sImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:param]]];
                    }
                }
            }
        }
        
        if(sImage)
        {
            // handle images
            recipePicture = [self scaleAndRotateImage:sImage];
            [self.picture setImage:recipePicture];
        }
    }
}

- (BOOL) canPerformAction:(SEL)selector withSender:(id) sender {
    if (selector == @selector(paste:)) {
        return YES;
    }
    return NO;
}

- (BOOL) canBecomeFirstResponder {
    return YES;
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Access the uncropped image from info dictionary
    recipePicture = [self scaleAndRotateImage:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
    [self.picture setImage:recipePicture];
    
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage *) scaleAndRotateImage: (UIImage *)image
{
    int kMaxResolution = 320; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

#pragma mark - Action Sheet delegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
                if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
                [self presentViewController:imagePicker animated:YES completion:nil];
                
                break;
                
            case 1:
                [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                [self presentViewController:imagePicker animated:YES completion:nil];
                break;
            default:
                break;
        }
    }
    else
    {
        switch (buttonIndex)
        {
            case 0:
                [self savePictureToAlbum];
                break;
            default:
                break;
        }
    }
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    [AppDelegate colorButtonsFromActionSheet:actionSheet];
}

# pragma mark - Core Data

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

# pragma mark - UITextView delegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView
{
   //self.webViewDetails.translatesAutoresizingMaskIntoConstraints = YES;
    
    switch (textView.tag)
    {
        case 1:
            if ([textView.text isEqualToString:@"Add title"])
                textView.text = @"";
            else
                [textView setTextColor:[UIColor blackColor]];
            break;
        case 2:
            if ([textView.text isEqualToString:@"Add ingredients"])
                textView.text = @"";
            else
                [textView setTextColor:[UIColor blackColor]];
            
            break;
        case 3:
        {
            if ([textView.text isEqualToString:@"Add instructions"])
                textView.text = @"";
            else
                [textView setTextColor:[UIColor blackColor]];
        }
    
        default:
            break;
    }
    
    // lift text view up
    CGPoint scrollPoint = CGPointMake(0.0, textView.frame.origin.y-5);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
    if (([self.segueIdentifier isEqualToString:@"copyPasteRecipe"] || [self.segueIdentifier isEqualToString:@"createRecipe"] || [self.navigationItem.rightBarButtonItem.title isEqualToString:@"Save"]) &&
        [textView.text isEqualToString:@""])
            [textView setTextColor:[UIColor blackColor]];
    
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    switch (textView.tag) {
        case 1:
            if ([textView.text isEqualToString:@""])
            {
                textView.text = @"Add title";
                [textView setTextColor:[UIColor lightGrayColor]];
            }
            else
                [textView setTextColor:[UIColor blackColor]];
            break;
        case 2:
            if ([textView.text isEqualToString:@""])
            {
                textView.text = @"Add ingredients";
                [textView setTextColor:[UIColor lightGrayColor]];
            }
            else
                [textView setTextColor:[UIColor blackColor]];
            break;
        case 3:
            if ([textView.text isEqualToString:@""])
            {
                textView.text = @"Add instructions";
                [textView setTextColor:[UIColor lightGrayColor]];
            }
            else
                [textView setTextColor:[UIColor blackColor]];
            break;
            
        default:
            break;
    }
    [textView resignFirstResponder];
}

#pragma mark - UITextField delegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //self.webViewDetails.translatesAutoresizingMaskIntoConstraints = YES;
    
    switch (textField.tag) {
        case 1:
            if ([textField.text isEqualToString:@"Add portions"])
            {
                textField.text = @"";
            }
            else
                [textField setTextColor:[UIColor blackColor]];
            break;
        case 2:
            if ([textField.text isEqualToString:@"Add time"])
            {
                textField.text = @"";
            }
            else
                [textField setTextColor:[UIColor blackColor]];
            break;
            
        default:
            break;
    }
    
    // lift text field up
    CGPoint scrollPoint = CGPointMake(0.0, textField.frame.origin.y-5);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
    if (([self.segueIdentifier isEqualToString:@"copyPasteRecipe"] || [self.segueIdentifier isEqualToString:@"createRecipe"] || [self.navigationItem.rightBarButtonItem.title isEqualToString:@"Save"]) &&
        [textField.text isEqualToString:@""])
        [textField setTextColor:[UIColor blackColor]];
    
    [textField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case 1:
            if ([textField.text isEqualToString:@""])
            {
                textField.text = @"Add portions";
                [textField setTextColor:[UIColor lightGrayColor]];
            }
            else
                [textField setTextColor:[UIColor blackColor]];
            break;
        case 2:
            if ([textField.text isEqualToString:@""])
            {
                textField.text = @"Add time";
                [textField setTextColor:[UIColor lightGrayColor]];
            }
            else
                [textField setTextColor:[UIColor blackColor]];
            break;
            
        default:
            break;
    }
    [textField resignFirstResponder];
}

#pragma mark - SAVE picture button

- (void)forwardButtonTouched
{
    if (SYSTEM_VERSION_LESS_THAN(@"8.0"))
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Save", nil];
        actionSheet.tag = 2;
        
        [actionSheet showInView:self.view];
    }
    else
    {
        
        UIAlertAction *save = [UIAlertAction
                                  actionWithTitle:@"Save"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      [self savePictureToAlbum];
                                  }];
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];

        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        [alertController addAction:save];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)savePictureToAlbum
{
    UIImageWriteToSavedPhotosAlbum(self.picture.image, self,  @selector(thisImage:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:), nil);
}

- (void)thisImage:(UIImage *)image hasBeenSavedInPhotoAlbumWithError:(NSError *)error usingContextInfo:(void*)ctxInfo {
    NSString *str;
    
    if (error)
    {
        // Do anything needed to handle the error or display it to the user
        str = @"Couldn't save photo to album. Check app's permissions in settings.";
    } else
    {
        str = @"Saved to photo album!";
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:str message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
@end
