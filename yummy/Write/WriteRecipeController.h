//
//  WriteRecipeController.h
//  yummy
//
//  Created by Dalia on 11/08/14.
//  Copyright (c) 2014 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "Recipe.h"

@protocol sendInsertedRecipe <NSObject>

-(void)sendRecipe:(Recipe *)recipe;
@end

@interface WriteRecipeController : UIViewController<UITextViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIWebViewDelegate, UIGestureRecognizerDelegate>
@property(nonatomic, assign) id del;
@property (strong,nonatomic) NSString *segueIdentifier;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *panGesture;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (weak, nonatomic) IBOutlet UITextView *ingredientsTextView;
@property (weak, nonatomic) IBOutlet UITextField *portions;
@property (weak, nonatomic) IBOutlet UITextField *time;
@property (weak, nonatomic) IBOutlet UITextView *howTo;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (weak, nonatomic) IBOutlet UIImageView *picture;
@property (weak, nonatomic) IBOutlet UITextView *name;
@property (weak, nonatomic) IBOutlet UIButton *goButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UITextField *address;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *webViewDetails;
@property (weak, nonatomic) IBOutlet UIView *backgroundActivityIndicator;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollViewContent;
@property (weak, nonatomic) IBOutlet UIView *backgroundPicture;

- (IBAction)editButtonActions:(id)sender;
- (IBAction)touchedPicture:(id)sender;
- (IBAction)go:(id)sender;
- (IBAction)panningDown:(id)sender;

@property (strong) Recipe *recipe;
@end
